
package Framework;


public class SecurityLayerException extends Exception {


    public SecurityLayerException(String message) {
        super(message);
    }

    public SecurityLayerException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityLayerException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return super.getMessage() + (getCause()!=null?" ("+getCause().getMessage()+")":"");                
    }

    
}
