package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Dao.Impl.AutostradaDAOIm;
import Model.Dao.Impl.CaselloDAOIm;

@WebServlet("/modifyautostrada")
public class modifyautostrada extends HttpServlet {
	
	/**
	 * Modifica un autostrada
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nameautostrada = request.getParameter("name");
		int tip = Integer.parseInt(request.getParameter("tip"));
		int id = Integer.parseInt(request.getParameter("x"));
		
		try {
			new AutostradaDAOIm().modifyAutostrada(nameautostrada, tip, id);
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
