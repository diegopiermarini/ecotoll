package Controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Bean.Casello;
import Model.Dao.Impl.AutostradaDAOIm;
import Model.Dao.Impl.CaselloDAOIm;

@WebServlet("/deleteautostrada")
public class deleteautostrada extends HttpServlet {
	
	
	/**
	 * Eliminazione di una autostrada con i relativi caselli
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idautostrada = Integer.parseInt(request.getParameter("idcasello"));
		
		try {
			
			
			List<Casello> listacaselliidautostrada = new CaselloDAOIm().getCaselliidautostrada(idautostrada);
			
			Iterator<Casello> itr = listacaselliidautostrada.iterator();
            while(itr.hasNext()) {
            	Casello casello= itr.next();
            	
            	new CaselloDAOIm().deleteCaselloforid(casello.getId_casello());
            	
            }
            
            new AutostradaDAOIm().removeAutostrada(idautostrada);
			
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
		
		
		
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
