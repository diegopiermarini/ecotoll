package Model.Bean;

public class Iva {
	
	int id_iva;
	double valore;
	
	/**
	 * Costruttore classe Iva
	 * Inizializzazione dei dati Iva presi dal DataBase per il calcolo del pedaggio
	 * 
	 * @param id_tariffa <b>(Int)</b>
	 * @param valore <b>(Double)</b>
	 */
	public Iva(int id_iva, double valore) {
		super();
		this.id_iva = id_iva;
		this.valore = valore;
	}

	public int getId_iva() {
		return id_iva;
	}

	public void setId_iva(int id_iva) {
		this.id_iva = id_iva;
	}

	public double getValore() {
		return valore;
	}

	public void setValore(double valore) {
		this.valore = valore;
	}
}
