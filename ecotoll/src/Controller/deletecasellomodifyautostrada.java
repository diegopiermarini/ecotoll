package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Bean.Casello;
import Model.Dao.Impl.CaselloDAOIm;

@WebServlet("/deletecasellomodifyautostrada")
public class deletecasellomodifyautostrada extends HttpServlet {
	
	/**
	 * Eliminazione di un casello all'interno della modifica del'autostrada 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idcasello = Integer.parseInt(request.getParameter("idcasello"));
		
		try {
			Casello cas = new CaselloDAOIm().getCaselloidcasello(idcasello);
			new CaselloDAOIm().deleteCaselloforid(idcasello);
			
			String z = String.valueOf(cas.getId_autostrada());
			
			response.setContentType("text/plain");
			response.getWriter().write(z.trim());
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
