package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Bean.Casello;
import Model.Dao.Impl.CaselloDAOIm;

@WebServlet("/insertcasellomodifyautostrada")
public class insertcasellomodifyautostrada extends HttpServlet {

	/**
	 * inserisce di un casello all'interno della modifica del'autostrada 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Casello cas = new Casello(request.getParameter("namecasellomodifyautostrada"), Float.valueOf(request.getParameter("kmcasellomodifyautostrada")), Integer.parseInt(request.getParameter("x")));
		
		try {
			new CaselloDAOIm().insertCasello(cas);
			Casello cas2 = new CaselloDAOIm().getCasello(cas);
			String z = String.valueOf(cas2.getId_casello());
			
			response.setContentType("text/plain");
			response.getWriter().write(z.trim());
			
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
