package Model.Dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Framework.DataLayerException;
import Framework.dbmanager;
import Model.Bean.Autostrada;
import Model.Bean.Tariffa;
import Model.Dao.Interface.TariffaDAO;

public class TariffaDAOIm implements TariffaDAO{
	
	private static final String get_tariffe = "SELECT * FROM tariffe";
	private static final String modifytariffa = "UPDATE tariffe SET A = ?, B = ?, tre = ?, quattro = ?, cinque = ? WHERE id_tariffa = ?";
	private static final String get_tariffabyid = "SELECT * FROM tariffe WHERE id_tariffa = ?";

	
	
	/**
	 *  Crea una lista delle Tariffe
	 *     
	 * @return lista di oggetti Tariffa <b>(List<Tariffa>)</b>
	 */
	public List<Tariffa> getTariffe() throws DataLayerException {
		List<Tariffa> ann = new ArrayList();		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_tariffe)){
				try(ResultSet rs = ps.executeQuery()){
						while(rs.next()) {
							ann.add(
									new Tariffa(
											rs.getInt("id_tariffa"),
											rs.getString("tipologia"),
											rs.getDouble("A"),
											rs.getDouble("B"),
											rs.getDouble("tre"),
											rs.getDouble("quattro"),
											rs.getDouble("cinque")
											)
							);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET TARIFFE", ex);
        }		
		return ann;
	}
	
	/**
	 *  Modifica una Tariffa   
	 *  
	 * @param id <b>(Int)</b>
	 * @param tarA <b>(Double)</b>
	 * @param tarB <b>(Double)</b>
	 * @param tar3 <b>(Double)</b>
	 * @param tar4 <b>(Double)</b>
	 * @param tar5 <b>(Double)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyTariffa(int id, double tarA, double tarB, double tar3, double tar4, double tar5) throws DataLayerException {
		int result = -1;
		
		try (Connection conn = dbmanager.getConnection()){
			
			try(PreparedStatement ps = conn.prepareStatement(modifytariffa)){
				ps.setDouble(1, tarA);
				ps.setDouble(2, tarB);
				ps.setDouble(3, tar3);
				ps.setDouble(4, tar4);
				ps.setDouble(5, tar5);
				ps.setInt(6, id);
				
				result=ps.executeUpdate();
			}
		}catch (SQLException e) {
			throw new DataLayerException("MODIFY TARIFFA", e);
		}
		return result;
	}
	
	/**
	 *  Crea oggetto Tariffa
	 * 
	 * @param id <b>(Int)</b>
	 *     
	 * @return oggetto Tariffa <b>(Tariffa)</b>
	 */
	public Tariffa getTariffabyid(int id) throws DataLayerException {
		Tariffa ann = null;				
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_tariffabyid)){
				ps.setInt(1, id);
				try(ResultSet rs = ps.executeQuery()){
						if(rs.next()) {
							ann = new Tariffa(
									rs.getInt("id_tariffa"),
									rs.getString("tipologia"),
									rs.getDouble("A"),
									rs.getDouble("B"),
									rs.getDouble("tre"),
									rs.getDouble("quattro"),
									rs.getDouble("cinque")
									);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET TARIFFA", ex);
        }		
		return ann;
	}

}
