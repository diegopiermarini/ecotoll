package Model.Bean;

import java.util.ArrayList;

public class Autostrada{
	
	String nome;
	int id;
	int tip;//pianuta/montagna
		
	/**
	 * Costruttore classe Autostrada
	 * Inizializzazione dei dati Autostrada presi dal template indexadmin o dal DataBase per la gestione dell'autostrada
	 * 
	 * @param id <b>(Int)</b>
	 * @param nome <b>(String)</b>
	 * @param tip <b>(Int)</b>
	 */
	public Autostrada(int id, String nome, int tip) {
		this.id = id;
		this.nome = nome;
		this.tip = tip;
	}

	/**
	 * Costruttore classe Autostrada
	 * Inizializzazione dei dati Autostrada presi dal template indexadmin per la gestione dell'autostrada
	 * 
	 * @param nome <b>(String)</b>
	 * @param tip <b>(Int)</b>
	 */
	public Autostrada(String nome, int tip) {
		this.nome = nome;
		this.tip = tip;

	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTip() {
		return tip;
	}

	public void setTip(int tip) {
		this.tip = tip;
	}
	
}
