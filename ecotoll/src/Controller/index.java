package Controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.TemplateManagerException;
import Framework.TemplateResult;
import Model.Bean.Admin;
import Model.Bean.Casello;
import Model.Bean.Veicolo;
import Model.Dao.Impl.AdminDAOIm;
import Model.Dao.Impl.CaselloDAOIm;
import Model.Dao.Impl.VeicoloDAOIm;
import Model.Dao.Interface.AdminDAO;
import Framework.DataLayerException;
import Framework.FailureResult;
import Framework.SecurityLayer;

public class index extends HttpServlet {
	
	/**
	 * Identifica la richiesta fatta
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DataLayerException {
		try {
            if (request.getParameter("login") != null) {
                login(request, response);
            } else {
                action_def(request, response);
            }
        } catch (IOException ex) {
            request.setAttribute("exception", ex);
            action_error(request, response);
        }
        
    }
	
	/**
	 * Solleva l'eccezione tramite il Framework
	 */
	private void action_error(HttpServletRequest request, HttpServletResponse response) {
        if (request.getAttribute("exception") != null) {
            (new FailureResult(getServletContext())).activate((Exception) request.getAttribute("exception"), request, response);
        } else {
            (new FailureResult(getServletContext())).activate((String) request.getAttribute("message"), request, response);
        }
    }
	
	/**
	 * Carica il Template index
	 * Crea le liste di oggetti i quali dati verranno caricati all'interno del Template index
	 */
	private void action_def(HttpServletRequest request, HttpServletResponse response) throws DataLayerException, ServletException{
		Map data = new HashMap();
        
        TemplateResult res = new TemplateResult(getServletContext()); 
		try {
				List<Casello> listacaselli = new CaselloDAOIm().getCaselli();
	            data.put("listacaselli", listacaselli);
	            
	            List<Veicolo> listaveicoli = new VeicoloDAOIm().getVeicoli();
	            data.put("listaveicoli", listaveicoli);
				
	            res.activate("index.ftl.html", data, response);
	        } catch (TemplateManagerException ex) {
	            throw new ServletException(ex);
	        }
    }  	

	/**
	 * Verifica le credenziali della login per il backend, crea la sessione e carica il Tamplate indexadmin
	 */
	private void login (HttpServletRequest request, HttpServletResponse response) throws IOException{
		try {	
		            String username = SecurityLayer.issetString("username", request.getParameter("username"));
		            String password = SecurityLayer.issetString("password", request.getParameter("psw"));
		            
		            try { //... VALIDAZIONE IDENTITA'...
		                
		                AdminDAO x = new AdminDAOIm();
		                Admin admin = x.Credenziali(username, password);
		               		                    	
		                if (admin != null) {
                            
                            SecurityLayer.createSession(request, admin.getUsername(), admin.getPassword(), "admin");
                            
                            if (request.getParameter("referrer") != null) {
                                response.sendRedirect(request.getParameter("referrer"));
                            } else {
                                response.sendRedirect("indexadmin");
                            }
                        } else {
                            //notifica errore credenziali
                        	Map data = new HashMap();
                            
                            TemplateResult res = new TemplateResult(getServletContext()); 
                    		try {
                    	            res.activate("index.ftl.html", data, response);
                    	        } catch (TemplateManagerException ex) {
                    	            throw new ServletException(ex);
                    	        }
                        }
		                           
		            } catch (Exception ex) {
		                request.setAttribute("exception", ex);
		                action_error(request, response);
		            }
		        } catch (Exception ex) {
		            request.setAttribute("exception", ex);
		            action_error(request, response);
		        }
				
		}
	
	/**
	 * doGet processRequest
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (DataLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * doPost processRequest
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (DataLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
