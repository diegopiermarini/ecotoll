package Model.Dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Framework.DataLayerException;
import Framework.dbmanager;
import Model.Bean.Iva;
import Model.Bean.Veicolo;
import Model.Dao.Interface.IvaDAO;

public class IvaDAOIm implements IvaDAO{
	
	private static final String getiva = "SELECT * FROM iva";
	private static final String modifyiva = "UPDATE iva SET valore = ? WHERE id_iva = 1";

	
	/**
	 *  Crea una oggetto Iva
	 *   
	 * @return oggetto Iva <b>(Iva)</b> 
	 */
	public Iva getIva() throws DataLayerException {
		Iva iva = null;		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(getiva)){
				try(ResultSet rs = ps.executeQuery()){
					if(rs.next()) {
						iva = new Iva(
								rs.getInt("id_iva"),
								rs.getDouble("valore")
								);
					}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET IVA", ex);
        }		
		return iva;
	}
	
	
	
	/**
	 *  Modifica l'Iva   
	 *  
	 * @param valore <b>(Double)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyIva(double valore) throws DataLayerException {
		int result = -1;
		
		try (Connection conn = dbmanager.getConnection()){
			
			try(PreparedStatement ps = conn.prepareStatement(modifyiva)){
				ps.setDouble(1, valore);
				
				
				result=ps.executeUpdate();
			}
		}catch (SQLException e) {
			throw new DataLayerException("MODIFY IVA", e);
		}
		return result;
	}
}
