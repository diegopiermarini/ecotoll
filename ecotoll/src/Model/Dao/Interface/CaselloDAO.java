package Model.Dao.Interface;

import java.util.List;

import Framework.DataLayerException;
import Model.Bean.Casello;

public interface CaselloDAO {
	
	/**
	 *  Inserisce un Casello   
	 *  
	 * @param a <b>(Casello)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int insertCasello(Casello a) throws DataLayerException;
	
	/**
	 *  Crea una lista di caselli di una determinata autostrada
	 *  
	 * @param idautostrada <b>(Int)</b>
	 * 
	 * @return lista di oggetti Casello <b>(List<Casello>)</b>
	 */
	public List<Casello> getCaselliidautostrada(int idautostrada) throws DataLayerException;
	
	/**
	 *  Elimina un Casello
	 *  
	 * @param id <b>(Int)</b>
	 * 
	 */
	public void deleteCaselloforid(int id) throws DataLayerException;
	
	/**
	 *  Crea una lista di tutti i caselli
	 *   
	 * @return lista di oggetti Casello <b>(List<Casello>)</b>
	 */
	public List<Casello> getCaselli() throws DataLayerException;
	
	/**
	 *  Crea una lista di tutti i caselli ordinati per km
	 *   
	 * @return lista di oggetti Casello <b>(List<Casello>)</b>
	 */
	public List<Casello> getCaselliorderkm() throws DataLayerException;
	
	/**
	 *  Crea un oggetto Casello preso dal DataBase 
	 *  
	 *  @param x <b>(Casello)</b>
	 *   
	 * @return oggetto Casello <b>(Casello)</b>
	 */
	public Casello getCasello(Casello x) throws DataLayerException;
	
	/**
	 *  Crea un oggetto Casello preso dal DataBase 
	 *  
	 *  @param id <b>(Int)</b>
	 *   
	 * @return oggetto Casello <b>(Casello)</b>
	 */
	public Casello getCaselloidcasello(int id) throws DataLayerException;
	
	/**
	 *  Modifica un Casello   
	 *  
	 * @param name <b>(String)</b>
	 * @param km <b>(Float)</b>
	 * @param idcasello <b>(Int)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyCasello(String name, float km, int idcasello) throws DataLayerException;
	
}
