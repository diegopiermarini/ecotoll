package Controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Framework.TemplateManagerException;
import Framework.TemplateResult;
import Model.Bean.Autostrada;
import Model.Bean.Casello;
import Model.Bean.Iva;
import Model.Bean.Tariffa;
import Model.Dao.Impl.AutostradaDAOIm;
import Model.Dao.Impl.CaselloDAOIm;
import Model.Dao.Impl.IvaDAOIm;
import Model.Dao.Impl.TariffaDAOIm;

public class indexadmin extends HttpServlet {
	
	/**
	 * Carica il Template indexadmin
	 * Crea le liste di oggetti i quali dati verranno caricati all'interno del Template indexadmin
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DataLayerException {

        try {
            Map data = new HashMap();
            
            TemplateResult res = new TemplateResult(getServletContext());
            
            List<Autostrada> listaautostrade = new AutostradaDAOIm().getAutostrade();
            data.put("listaautostrade", listaautostrade);
            
            List<Casello> listacaselli = new CaselloDAOIm().getCaselli();
            data.put("listacaselli", listacaselli);
            
            List<Casello> listacaselliorderkm = new CaselloDAOIm().getCaselliorderkm();
            data.put("listacaselliorderkm", listacaselliorderkm);
            
            List<Tariffa> listat = new TariffaDAOIm().getTariffe();
            data.put("listat", listat);
            
            Iva iva = new IvaDAOIm().getIva();
            data.put("iva", iva);          
                     
            res.activate("indexadmin.ftl.html", data, response);
        } catch (TemplateManagerException ex) {
            throw new ServletException(ex);
        }
    }
	

	/**
	 * doGet processRequest
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (DataLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * doPost processRequest
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (DataLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
