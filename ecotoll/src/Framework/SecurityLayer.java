package Framework;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.IllegalFormatException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SecurityLayer {

    public static HttpSession checkSession(HttpServletRequest r) {
        boolean check = true;

        HttpSession s = r.getSession(false);
        
        if (s == null) {
            return null;
        }

        
        if (s.getAttribute("userid") == null) {
            check = false;
            
        } else if ((s.getAttribute("ip") == null) || !((String) s.getAttribute("ip")).equals(r.getRemoteHost())) {
            check = false;
            
        } else {
            
            Calendar begin = (Calendar) s.getAttribute("inizio-sessione");
            
            Calendar last = (Calendar) s.getAttribute("ultima-azione");
           
            Calendar now = Calendar.getInstance();
            if (begin == null) {
                check = false;
            } else {
                
                long secondsfrombegin = (now.getTimeInMillis() - begin.getTimeInMillis()) / 1000;
                
                if (secondsfrombegin > 3 * 60 * 60) {
                    check = false;
                } else if (last != null) {
                    
                    long secondsfromlast = (now.getTimeInMillis() - last.getTimeInMillis()) / 1000;
                                     
                    if (secondsfromlast > 30 * 60) {
                        check = false;
                    }
                }
            }
        }
        if (!check) {
            s.invalidate();
            return null;
        } else {
            
            s.setAttribute("ultima-azione", Calendar.getInstance());
            return s;
        }
    }

    public static HttpSession createSession(HttpServletRequest request, String username, String email, String tipo) {
        HttpSession s = request.getSession(true);
        s.setAttribute("username", username);
        s.setAttribute("email", email);
        s.setAttribute("tipo", tipo);
        s.setAttribute("ip", request.getRemoteHost());
        s.setAttribute("inizio-sessione", Calendar.getInstance());
        
        return s;
    }

    public static void disposeSession(HttpServletRequest request) {
        HttpSession s = request.getSession(false);
        if (s != null) {
            s.invalidate();
        }
    }

    
    public static String addSlashes(String s) {
        return s.replaceAll("(['\"\\\\])", "\\\\$1");
    }

    
    public static String stripSlashes(String s) {
        return s.replaceAll("\\\\(['\"\\\\])", "$1");
    }

    public static int checkNumeric(String s) throws NumberFormatException {
        
        if (s != null) {
            
            return Integer.parseInt(s);
        } else {
            throw new NumberFormatException("String argument is null");
        }
    }

    public static LocalDate issetDate(String parameter, String date) throws SecurityLayerException {
        
        if (date != null) {
            try {
                return LocalDate.parse(date);
            } catch (IllegalArgumentException ex) {
                throw new SecurityLayerException("Formato dati errato");
            }
        } else {
            throw new SecurityLayerException("Parametro obbligatorio: " + parameter);
        }
    }

    public static int issetInt(String s) throws SecurityLayerException {

        if (s != null) {
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                throw new SecurityLayerException("Formato dati non valido");
            }
        } else {
            throw new SecurityLayerException("Richiesta non valida");
        }
    }
    
    public static int issetInt(String parameter, String s) throws SecurityLayerException {

        if (s != null) {
            try {
                return Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                throw new SecurityLayerException("Formato dati non valido");
            }
        } else {
            throw new SecurityLayerException("Parametro obbligatorio: "+ parameter);
        }
    }

    public static String issetString(String s) throws SecurityLayerException {
        if (s != null) {
            return s;
        } else {
            throw new SecurityLayerException("Richiesta non valida");
        }
    }

    public static String issetString(String parameter, String s) throws SecurityLayerException {
        if (s != null) {
            if (!s.isEmpty()) {
                return s;
            } else {
                throw new SecurityLayerException("Parametro vuoto: " + parameter);
            }
        } else {
            throw new SecurityLayerException("Parametro obbligatorio: " + parameter);
        }
    }

    public static boolean checkString(String s) throws IllegalArgumentException {
        if (s != null) {
            return true;
        } else {
            throw new IllegalArgumentException("String argument is null");
        }
    }

    public static LocalDate checkDate(String date) throws IllegalArgumentException {
        if (date != null) {
            return LocalDate.parse(date);
        } else {
            throw new IllegalArgumentException("Date wrong");
        }
    }

    
    public static int checkPage(String s) throws SecurityLayerException {
        try {
            if (s != null) {
                int page = Integer.parseInt(s);

                if (page <= 0) {
                    return 0;
                }

                return page;
            } else {
                return 0;
            }
        } catch (NumberFormatException ex) {
            throw new SecurityLayerException("Errore formato");
        }
    }
    
    public static int checkNumericPage(String s) throws NumberFormatException {

        if (s != null) {
            int page = Integer.parseInt(s);

            if (page <= 0) {
                return 0;
            }

            return page;
        } else {
            return 0;
        }
    }

    public static boolean checkHttps(HttpServletRequest r) {
        return r.isSecure();
       
    }

    
    public static void redirectToHttps(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String server = request.getServerName();
        String context = request.getContextPath();
        String path = request.getServletPath();
        String info = request.getPathInfo();
        String query = request.getQueryString();

        String newUrl = "https://" + server + ":8443" + context + path + (info != null ? info : "") + (query != null ? "?" + query : "");
        try {
            
            response.sendRedirect(newUrl);
        } catch (IOException ex) {
            try {
                
                response.sendError(response.SC_INTERNAL_SERVER_ERROR, "Cannot redirect to HTTPS, blocking request");
            } catch (IOException ex1) {
                
                throw new ServletException("Cannot redirect to https!");
            }
        }
    }
}
