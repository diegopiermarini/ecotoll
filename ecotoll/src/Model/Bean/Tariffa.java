package Model.Bean;

public class Tariffa {
	int id_tariffa;
	String tipologia; 
	double a, b, tre, quattro, cinque;
	
	/**
	 * Costruttore classe Tariffa
	 * Inizializzazione dei dati Tariffa presi dal DataBase per il calcolo del pedaggio
	 * 
	 * @param id_tariffa <b>(Int)</b>
	 * @param tipologia <b>(String)</b>
	 * @param a <b>(Double)</b>
	 * @param b <b>(Double)</b>
	 * @param tre <b>(Double)</b>
	 * @param quattro <b>(Double)</b>
	 * @param cinque <b>(Double)</b>
	 */
	public Tariffa(int id_tariffa, String tipologia, double a, double b, double tre, double quattro, double cinque) {
		super();
		this.id_tariffa = id_tariffa;
		this.tipologia = tipologia;
		this.a = a;
		this.b = b;
		this.tre = tre;
		this.quattro = quattro;
		this.cinque = cinque;
	}
	public int getId_tariffa() {
		return id_tariffa;
	}
	public void setId_tariffa(int id_tariffa) {
		this.id_tariffa = id_tariffa;
	}
	public String getTipologia() {
		return tipologia;
	}
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		this.b = b;
	}
	public double getTre() {
		return tre;
	}
	public void setTre(double tre) {
		this.tre = tre;
	}
	public double getQuattro() {
		return quattro;
	}
	public void setQuattro(double quattro) {
		this.quattro = quattro;
	}
	public double getCinque() {
		return cinque;
	}
	public void setCinque(double cinque) {
		this.cinque = cinque;
	}
	
	
	
	

}
