package Model.Dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Framework.DataLayerException;
import Framework.dbmanager;
import Model.Bean.Autostrada;
import Model.Bean.Casello;
import Model.Bean.Veicolo;
import Model.Dao.Interface.VeicoloDAO;

public class VeicoloDAOIm implements VeicoloDAO {
	
	private static final String get_veicoli = "SELECT * FROM veicolo";
	private static final String get_veicolibyid = "SELECT * FROM veicolo WHERE id_veicolo = ?";
	
	/**
	 *  Crea una lista di tutti i Veicoli
	 * 
	 * @return lista di oggetti Veicolo <b>(List<Veicolo>)</b> 
	 */
	public List<Veicolo> getVeicoli() throws DataLayerException {
		List<Veicolo> auto = new ArrayList();		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_veicoli)){
				try(ResultSet rs = ps.executeQuery()){
						while(rs.next()) {
							auto.add(
									new Veicolo(
											rs.getInt("id_veicolo"),
											rs.getString("modello"),
											rs.getString("marca"),
											rs.getInt("anno"),
											rs.getString("targa"),
											rs.getInt("assi"),
											rs.getInt("peso"),
											rs.getInt("altezza")
											)
							);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET VEICOLI", ex);
        }		
		return auto;
	}
	
	/**
	 *  Crea una oggetto Veicolo
	 *  
	 *  @param id <b>(Int)</b>
	 * 
	 * @return oggetto Veicolo <b>(Veicolo)</b> 
	 */
	public Veicolo getVeicolobyid(int id) throws DataLayerException {
		Veicolo auto = null;		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_veicolibyid)){
				ps.setInt(1, id);
				try(ResultSet rs = ps.executeQuery()){
					if(rs.next()) {
						auto = new Veicolo(
								rs.getInt("id_veicolo"),
								rs.getString("modello"),
								rs.getString("marca"),
								rs.getInt("anno"),
								rs.getString("targa"),
								rs.getInt("assi"),
								rs.getInt("peso"),
								rs.getInt("altezza")
								);
					}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET VEICOLO", ex);
        }		
		return auto;
	}
		
	

}
