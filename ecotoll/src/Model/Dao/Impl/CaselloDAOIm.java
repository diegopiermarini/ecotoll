package Model.Dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Framework.DataLayerException;
import Framework.dbmanager;
import Model.Bean.Autostrada;
import Model.Bean.Casello;
import Model.Dao.Interface.CaselloDAO;

public class CaselloDAOIm implements CaselloDAO {
	
	private static final String insert_casello = "INSERT INTO caselli (nome_casello, km_casello, autostrada_idautostrada) VALUES (?,?,?)";
	
	private static final String get_caselliidautostrada = "SELECT * FROM caselli WHERE autostrada_idautostrada = ?";
	
	private static final String deletecasellofroid = "DELETE FROM caselli WHERE id_casello = ?";
	
	private static final String get_caselli = "SELECT * FROM caselli";
	
	private static final String get_caselliorderkm = "SELECT * FROM caselli ORDER BY km_casello";
	
	private static final String get_casello = "SELECT * FROM caselli WHERE nome_casello = ? AND km_casello = ? AND autostrada_idautostrada = ?";
	
	private static final String get_caselloidcasello = "SELECT * FROM caselli WHERE id_casello = ?";

	private static final String modifycasello = "UPDATE caselli SET nome_casello = ?, km_casello = ? WHERE id_casello = ?";

	/**
	 *  Inserisce un Casello   
	 *  
	 * @param a <b>(Casello)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int insertCasello(Casello a) throws DataLayerException {
		int result = -1;
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(insert_casello)){
				ps.setString(1, a.getNome());
				ps.setFloat(2, a.getKm());
				ps.setInt(3, a.getId_autostrada());
				result = ps.executeUpdate();
			}
		}catch (SQLException ex) {
            throw new DataLayerException("INSERT CASELLO", ex);
        }	
		return result;
	}
	
	/**
	 *  Crea una lista di caselli di una determinata autostrada
	 *  
	 * @param idautostrada <b>(Int)</b>
	 * 
	 * @return lista di oggetti Casello <b>(List<Casello>)</b>
	 */
	public List<Casello> getCaselliidautostrada(int idautostrada) throws DataLayerException {
		List<Casello> ann = new ArrayList();		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_caselliidautostrada)){
				ps.setInt(1, idautostrada);
				try(ResultSet rs = ps.executeQuery()){
						while(rs.next()) {
							ann.add(
									new Casello(
											rs.getInt("id_casello"),
											rs.getString("nome_casello"),
											rs.getFloat("km_casello"),
											rs.getInt("autostrada_idautostrada")
											)
							);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET CASELLI FOR ID AUTOSTRADA", ex);
        }		
		return ann;
	}
	
	/**
	 *  Elimina un Casello
	 *  
	 * @param id <b>(Int)</b>
	 * 
	 */
	public void deleteCaselloforid(int id) throws DataLayerException {	
		try(Connection conn = dbmanager.getConnection()){
			try(PreparedStatement ps = conn.prepareStatement(deletecasellofroid)){
				ps.setInt(1, id);
				ps.executeUpdate();
			}
		}catch(SQLException ex) {
			throw new DataLayerException("DELETE CASELLO", ex);
		}
	}
	
	/**
	 *  Crea una lista di tutti i caselli
	 *   
	 * @return lista di oggetti Casello <b>(List<Casello>)</b>
	 */
	public List<Casello> getCaselli() throws DataLayerException {
		List<Casello> ann = new ArrayList();		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_caselli)){
				try(ResultSet rs = ps.executeQuery()){
						while(rs.next()) {
							ann.add(
									new Casello(
											rs.getInt("id_casello"),
											rs.getString("nome_casello"),
											rs.getFloat("km_casello"),
											rs.getInt("autostrada_idautostrada")
											)
							);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET CASELLI", ex);
        }		
		return ann;
	}
	
	/**
	 *  Crea una lista di tutti i caselli ordinati per km
	 *   
	 * @return lista di oggetti Casello <b>(List<Casello>)</b>
	 */
	public List<Casello> getCaselliorderkm() throws DataLayerException {
		List<Casello> ann = new ArrayList();		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_caselliorderkm)){
				try(ResultSet rs = ps.executeQuery()){
						while(rs.next()) {
							ann.add(
									new Casello(
											rs.getInt("id_casello"),
											rs.getString("nome_casello"),
											rs.getFloat("km_casello"),
											rs.getInt("autostrada_idautostrada")
											)
							);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET CASELLI", ex);
        }		
		return ann;
	}
	
	/**
	 *  Crea un oggetto Casello preso dal DataBase 
	 *  
	 *  @param x <b>(Casello)</b>
	 *   
	 * @return oggetto Casello <b>(Casello)</b>
	 */
	public Casello getCasello(Casello x) throws DataLayerException {
		Casello cas = null;		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_casello)){
				ps.setString(1, x.getNome());
				ps.setFloat(2, x.getKm());
				ps.setInt(3, x.getId_autostrada());
				try(ResultSet rs = ps.executeQuery()){
						if(rs.next()) {
							cas = new Casello(
									rs.getInt("id_casello"),
									rs.getString("nome_casello"),
									rs.getFloat("km_casello"),
									rs.getInt("autostrada_idautostrada")
									);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET CASELLO", ex);
        }		
		return cas;
	}
	
	/**
	 *  Crea un oggetto Casello preso dal DataBase 
	 *  
	 *  @param id <b>(Int)</b>
	 *   
	 * @return oggetto Casello <b>(Casello)</b>
	 */
	public Casello getCaselloidcasello(int id) throws DataLayerException {
		Casello cas = null;		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_caselloidcasello)){
				ps.setInt(1, id);
				try(ResultSet rs = ps.executeQuery()){
						if(rs.next()) {
							cas = new Casello(
									rs.getInt("id_casello"),
									rs.getString("nome_casello"),
									rs.getFloat("km_casello"),
									rs.getInt("autostrada_idautostrada")
									);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET CASELLO ID CASELLO", ex);
        }		
		return cas;
	}
	
	/**
	 *  Modifica un Casello   
	 *  
	 * @param name <b>(String)</b>
	 * @param km <b>(Float)</b>
	 * @param idcasello <b>(Int)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyCasello(String name, float km, int idcasello) throws DataLayerException {
		int result = -1;
		
		try (Connection conn = dbmanager.getConnection()){
			
			try(PreparedStatement ps = conn.prepareStatement(modifycasello)){
				ps.setString(1, name);
				ps.setFloat(2, km);
				ps.setInt(3, idcasello);
				
				result=ps.executeUpdate();
			}
		}catch (SQLException e) {
			throw new DataLayerException("MODIFY CASELLO", e);
		}
		return result;
	}
	
	
	

}