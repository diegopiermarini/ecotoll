package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Dao.Impl.IvaDAOIm;
import Model.Dao.Impl.TariffaDAOIm;

@WebServlet("/modifyiva")
public class modifyiva extends HttpServlet {
	
	/**
	 * modifica dell'iva 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		double valore = Double.parseDouble(request.getParameter("valore"));
		try {
			new IvaDAOIm().modifyIva(valore);
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
