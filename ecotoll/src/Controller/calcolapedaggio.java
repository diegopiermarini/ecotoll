package Controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Bean.Autostrada;
import Model.Bean.Casello;
import Model.Bean.Iva;
import Model.Bean.Tariffa;
import Model.Bean.Veicolo;
import Model.Dao.Impl.AutostradaDAOIm;
import Model.Dao.Impl.CaselloDAOIm;
import Model.Dao.Impl.IvaDAOIm;
import Model.Dao.Impl.TariffaDAOIm;
import Model.Dao.Impl.VeicoloDAOIm;

@WebServlet("/calcolapedaggio")
public class calcolapedaggio extends HttpServlet {
   
	/**
	 * Calcola il pedaggio
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id_ing = Integer.parseInt(request.getParameter("id_ing"));
		int id_usc = Integer.parseInt(request.getParameter("id_usc"));
		int id_vei = Integer.parseInt(request.getParameter("id_vei"));
		String z = null;
		
		try {
			Casello cas1 = new CaselloDAOIm().getCaselloidcasello(id_ing);
			Casello cas2 = new CaselloDAOIm().getCaselloidcasello(id_usc);
			
			if(cas1.getId_autostrada() == cas2.getId_autostrada()) 
			// caso in cui i caselli inseriti appartengono alla stessa autostrada
			{
				Autostrada autostrada = new AutostradaDAOIm().getAutostradabyid(cas1.getId_autostrada());
				
				Veicolo veicolo = new VeicoloDAOIm().getVeicolobyid(id_vei);
				String classe_veicolo = SetClasse(veicolo);
				
				Iva iva = new IvaDAOIm().getIva();
				
				Tariffa tariffaitr = new TariffaDAOIm().getTariffabyid(autostrada.getTip());
				
				double tariffa = SetTariffaUnitaria(tariffaitr, classe_veicolo);
				
				double pedaggio = Math.abs(cas1.getKm() - cas2.getKm());
				pedaggio *= tariffa; 
				pedaggio += ((pedaggio/100)*iva.getValore());
				z = Double.toString(Arrotondamento(pedaggio));
			}
			else 
			// caso in cui i caselli inseriti non appartengono alla stessa autostrada
			{
				z = "I Caselli inseriti non appartengono alla stessa Autostrada!";
			}
			
			response.setContentType("text/plain");
			response.getWriter().write(z.trim());

		} catch (DataLayerException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}
	
	/**
	 * metodo per determinare la tipologia del veicolo
	 */
	public String SetClasse(Veicolo a) {
			
			String classe = "";
			int assi = a.getAssi();
			if(assi>=5)assi=5;
			double altezza = a.getAltezza();
			switch(assi) {
			case 0://Motocicli
				classe="A";
				break;
			case 2://Veicoli a 2 assi 
				if(altezza<=1.30) 
				{
					classe="A";// altezza minore/uguale a m. 1,30 
				}
				else 
				{
					classe="B";// altezza > m. 1,30 
				}
				break;
			case 3://Veicoli e convogli costruiti a 3 assi 
				classe="3";
				break;
			case 4://Veicoli e convogli costruiti a 4 assi.
				classe="4";
				break;
			case 5://Veicoli e convogli costruiti a 5 o pi� assi. 
				classe="5";
				break;
			}
			
			return classe;	
	}
	
	/**
	 * metodo per prende la tarriffa unitaria in base alla tipologia del veiocolo 
	 */
	public double SetTariffaUnitaria(Tariffa tariffa, String classe) {
			
			double tar = 0;
			
			switch(classe) {
			case "A"://classe veicolo A
				tar = tariffa.getA();
				break;
			case "B"://classe veicolo B
				tar = tariffa.getB();
				break;
			case "3"://classe veicolo 3
				tar = tariffa.getTre();
				break;
			case "4"://classe veicolo 4
				tar = tariffa.getQuattro();
				break;
			case "5"://classe veicolo 5
				tar = tariffa.getCinque();
				break;
			}
			return tar;	
	}	
	
	/**
	 * metodo per arrotondare per eccesso o per difetto l'importo ottenuto
	 */
	public double Arrotondamento(double a) {
		
		double rounded1 = Math.round(a * 10) / 10.0;
		
		return rounded1;
	}
}
