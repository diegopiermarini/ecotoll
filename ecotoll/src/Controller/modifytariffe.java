package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Dao.Impl.TariffaDAOIm;

@WebServlet("/modifytariffe")
public class modifytariffe extends HttpServlet {
	
	/**
	 * modifica delle tariffe 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		double tarA = Double.parseDouble(request.getParameter("tarA"));
		double tarB = Double.parseDouble(request.getParameter("tarB"));
		double tar3 = Double.parseDouble(request.getParameter("tar3"));
		double tar4 = Double.parseDouble(request.getParameter("tar4"));
		double tar5 = Double.parseDouble(request.getParameter("tar5"));
		try {
			new TariffaDAOIm().modifyTariffa(id, tarA, tarB, tar3, tar4, tar5);
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
