package Model.Bean;

public class Casello {
	String nome;
	float km;
	int id_casello, id_autostrada; 
	
	/**
	 * Costruttore classe Casello
	 * Inizializzazione dei dati Casello presi dal template indexadmin o dal DataBase per la gestione del casello
	 * 
	 * @param nome <b>(String)</b>
	 * @param km <b>(Float)</b>
	 */
	public Casello(String nome, float km) {
		this.nome=nome;
		this.km=km;
	}
	
	/**
	 * Costruttore classe Casello
	 * Inizializzazione dei dati Casello presi dal template indexadmin o dal DataBase per la gestione del casello
	 * 
	 * @param nome <b>(String)</b>
	 * @param km <b>(Float)</b>
	 * @param id_autostrada <b>(Int)</b>
	 */
	public Casello(String nome, float km, int id_autostrada) {
		this.nome=nome;
		this.km=km;
		this.id_autostrada=id_autostrada;
	}
	
	/**
	 * Costruttore classe Casello
	 * Inizializzazione dei dati Casello presi dal template indexadmin o dal DataBase per la gestione del casello
	 * 
	 * @param id_casello <b>(Int)</b>
	 * @param nome <b>(String)</b>
	 * @param km <b>(Float)</b>
	 * @param id_autostrada <b>(Int)</b>
	 */
	public Casello(int id_casello, String nome, float km, int id_autostrada) {
		this.id_casello=id_casello;
		this.nome=nome;
		this.km=km;
		this.id_autostrada=id_autostrada;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public float getKm() {
		return km;
	}

	public void setKm(float km) {
		this.km = km;
	}

	public int getId_casello() {
		return id_casello;
	}

	public void setId_casello(int id_casello) {
		this.id_casello = id_casello;
	}

	public int getId_autostrada() {
		return id_autostrada;
	}

	public void setId_autostrada(int id_autostrada) {
		this.id_autostrada = id_autostrada;
	}
	

}
