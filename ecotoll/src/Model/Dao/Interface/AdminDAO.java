package Model.Dao.Interface;

import Framework.DataLayerException;
import Model.Bean.Admin;
import java.io.InputStream;
import java.util.List;

public interface AdminDAO {

	/**
	 *  Controlla se le credenziali se sono giuste per la login al backend
	 * 
	 * @param username <b>(String)</b>
	 * @param password <b>(String)</b>
	 * 
	 * @return oggetto Admin <b>(Admin)</b> se le credenziali sono giuste
	 */
    public Admin Credenziali(String username, String password) throws DataLayerException;
}
