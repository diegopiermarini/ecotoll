package Model.Dao.Interface;

import Framework.DataLayerException;
import Model.Bean.Iva;

public interface IvaDAO {
	
	/**
	 *  Crea una oggetto Iva
	 *   
	 * @return oggetto Iva <b>(Iva)</b> 
	 */
	public Iva getIva() throws DataLayerException;
	
	/**
	 *  Modifica l'Iva   
	 *  
	 * @param valore <b>(Double)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyIva(double valore) throws DataLayerException;

}
