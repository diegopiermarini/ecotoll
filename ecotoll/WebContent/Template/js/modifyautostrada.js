//visualizzare il modal per modificare autostrada
function viewmodalmodifyautostrada(x){
	
	document.getElementById(x+'autostradamodify').style.display='block';
}
//insert Casello in modify autostrada
function insertcaselloinmodifyautostrada(x){
	
	var namecasellomodifyautostrada = document.getElementById(x+'namecasellomodifyautostrada').value;
	var kmcasellomodifyautostrada = document.getElementById(x+'kmcasellomodifyautostrada').value;
	
	if(namecasellomodifyautostrada === '' || kmcasellomodifyautostrada === '')
	{
		alert("Inserire tutti i campi per inserre un Casello!");
	}
	else
	{
		$.ajax({
			url : 'insertcasellomodifyautostrada',
			data : {namecasellomodifyautostrada, kmcasellomodifyautostrada, x},
			success : function(responseText) {
		
				document.getElementById(x+'namecasellomodifyautostrada').value = "";
				document.getElementById(x+'kmcasellomodifyautostrada').value = "";
				
				//nella table modify autostrada
				var table = document.getElementById(x+'tablecaselliautostradamodify');
				var row = table.insertRow(1);
				row.id = responseText+'autostradamodify';
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
				var cell3 = row.insertCell(2);
				cell1.id = responseText+'nameaotostradamodify';
				cell2.id = responseText+'kmaotostradamodify';
				cell1.innerHTML = namecasellomodifyautostrada;
				cell2.innerHTML = kmcasellomodifyautostrada;
				cell3.innerHTML = "<i class='fa fa-edit' id='"+responseText+"' style='font-size:24px' title='Modify' onclick='viewmodalmodifycasellomodifyautostrada(this.id)'></i>&emsp; <i class='fa fa-close' id='"+cell1.id+"' style='font-size:24px' title='Delete' onclick='viewmodaldeletecasellomodifyautostrada(this.id)'></i>";		
			
				//nella table caselli
				var table2 = document.getElementById("myTablecaselli");
				var row2 = table2.insertRow(1);
				row2.id = responseText+'casello';
				var cell4 = row2.insertCell(0);
				var cell5 = row2.insertCell(1);
				var cell6 = row2.insertCell(2);
				var cell7 = row2.insertCell(3);
				cell4.id = responseText+'casellonome';
				cell5.id = responseText+'casellokm';
				cell4.innerHTML = namecasellomodifyautostrada;
				cell5.innerHTML = kmcasellomodifyautostrada;
				cell6.innerHTML = document.getElementById(x+'nameautostradamodify').value;
				cell7.innerHTML = "<i class='fa fa-edit' id='"+responseText+"' style='font-size:24px' title='Modify' onclick='viewmodalmodifycasello(this.id)'></i>&emsp; <i class='fa fa-close' id='"+cell4.id+"' style='font-size:24px' title='Delete' onclick='viewmodaldeletecasello(this.id)'></i>";
			}
		});	
	}
}
//var di appoggio
var iddelete = "";
//visualizzare il modal per eliminare il casello in modify autostrada
function viewmodaldeletecasellomodifyautostrada(x){
	
	var name = $('#'+x).text();
	document.getElementById('deletecasellomodifyautostrada').style.display='block';
	document.getElementById("namecasellodeletemodifyautostrada").innerHTML = "Do you really want to delete these Casello <u style='color:red;'>"+name+"</u> ?";
	iddelete = x;
}
//elimina un casello in modify autostrada
function deletecaselomodifyautostrada(){
	
	var idcasello = iddelete.replace(/nameaotostradamodify/,"");
	$.ajax({
		url : 'deletecasellomodifyautostrada',
		data : {idcasello},
		success : function(responseText) {
			
			$('table#'+responseText+'tablecaselliautostradamodify tr#'+idcasello+'autostradamodify').remove();
			$('table#myTablecaselli tr#'+idcasello+'casello').remove();
			
			document.getElementById('deletecasellomodifyautostrada').style.display='none';
		}
	
	});	
	
}
//var di appoggio
var idmodify = "";
//visualizzare il modal per modificare il casello in modify autostrada
function viewmodalmodifycasellomodifyautostrada(y){
	
	document.getElementById('modifycasellomodifyautostrada').style.display='block';
	document.getElementById('namecasellomodifymodifyautostrada').value = $('#'+y+'nameaotostradamodify').text();
	document.getElementById('kmcasellomodifymodifyautostrada').value = $('#'+y+'kmaotostradamodify').text();
	idmodify = y;
}
//conferma le modificare del casello in modify autostrada
function confermamodifichecaselloinsertautostrada(){
	
	if(document.getElementById('namecasellomodifymodifyautostrada').value === '' || document.getElementById('kmcasellomodifymodifyautostrada').value === '')
	{
		alert("Inserire tutti i campi per modificare il Casello!");
	}
	else
	{
		var name = document.getElementById('namecasellomodifymodifyautostrada').value;
		var km = document.getElementById('kmcasellomodifymodifyautostrada').value
		$.ajax({
			url : 'modifycasellomodifyautostrada',
			data : {name, km, idmodify},
			success : function(responseText) {
				
				document.getElementById('modifycasellomodifyautostrada').style.display='none';
				
				$('table#'+responseText+'tablecaselliautostradamodify tr#'+idmodify+'autostradamodify td#'+idmodify+'nameaotostradamodify').html(name);
				$('table#'+responseText+'tablecaselliautostradamodify tr#'+idmodify+'autostradamodify td#'+idmodify+'kmaotostradamodify').html(km);
				
				$('table#myTablecaselli tr#'+idmodify+'casello td#'+idmodify+'casellonome').html(name);
				$('table#myTablecaselli tr#'+idmodify+'casello td#'+idmodify+'casellokm').html(km);
			}
		
		});
	}
}
//conferma modify autostrada
function confermamodifyautostrada(x){
	var name = document.getElementById(x+'nameautostradamodify').value;
	var tip = document.getElementById(x+'typologyautostradamodify').value;
	if($('#'+x+'tablecaselliautostradamodify tr').length < 3 || name === '')
	{
		alert("Inserisci tutti i dati per apportare modifiche all' Autostrada!");
	}
	else
	{
		$.ajax({
			url : 'modifyautostrada',
			data : {name, tip, x},
			success : function() {location.reload();}
		});	
	}
}































