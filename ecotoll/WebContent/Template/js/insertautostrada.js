//var di appoggio
var r = 0;
//insert Casello in insert autostrada
function insertcaselloininsertautostrada(){
	
	var namecaselloinsertautostrada = document.getElementById("namecaselloinsertautostrada").value;
	var kmcaselloinsertautostrada = document.getElementById("kmcaselloinsertautostrada").value;
	
	if(namecaselloinsertautostrada === '' || kmcaselloinsertautostrada === '')
	{
		alert("Inserire tutti i campi per inserre un Casello!");
	}
	else
	{
		document.getElementById("namecaselloinsertautostrada").value = "";
		document.getElementById("kmcaselloinsertautostrada").value = "";
		
		var table = document.getElementById("tableinsertcaselliinsertautostrada");
		var row = table.insertRow(1);
		row.id = r+'idmomentaneocaselloinsertautostrada';
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		var cell3 = row.insertCell(2);
		cell1.id = r+'idmomentaneonomecaselloinsertautostrada';
		cell2.id = r+'idmomentaneokmcaselloinsertautostrada';
		cell1.innerHTML = namecaselloinsertautostrada;
		cell2.innerHTML = kmcaselloinsertautostrada;
		cell3.innerHTML = "<i class='fa fa-edit' id='"+cell1.id+"' style='font-size:24px' title='Modify' onclick='viewmodalmodifycaselloinsertautostrada(this.id)'></i>&emsp; <i class='fa fa-close' id='"+cell1.id+"' style='font-size:24px' title='Delete' onclick='viewmodaldeletecaselloinsertautostrada(this.id)'></i>";	
		r++;
	}
}
//var di appoggio
var iddelete = "";
//visualizzare il modal per eliminare il casello in insert autostrada
function viewmodaldeletecaselloinsertautostrada(x){
	
	var name = $('#'+x).text();
	document.getElementById('deletecaselloinsertautostrada').style.display='block';
	document.getElementById("namecasellodeleteinsertautostrada").innerHTML = "Do you really want to delete these Casello <u style='color:red;'>"+name+"</u> ?";
	iddelete = x;
}
//elimina un casello in insert autostrada
function deletecaseloinsertautostrada(){
	
	var newStr = iddelete.replace(/idmomentaneonomecaselloinsertautostrada/,"").concat("idmomentaneocaselloinsertautostrada");
	$('table#tableinsertcaselliinsertautostrada tr#'+newStr).remove();
	document.getElementById('deletecaselloinsertautostrada').style.display='none';
}
//var di appoggio
var idmodify = "";
//visualizzare il modal per modificare il casello in insert autostrada
function viewmodalmodifycaselloinsertautostrada(y){
	
	var newStr = y.replace(/idmomentaneonomecaselloinsertautostrada/,"")
	document.getElementById('modifycaselloinsertautostrada').style.display='block';
	document.getElementById('namecasellomodifyinsertautostrada').value = $('#'+newStr+'idmomentaneonomecaselloinsertautostrada').text();
	document.getElementById('kmcasellomodifyinsertautostrada').value = $('#'+newStr+'idmomentaneokmcaselloinsertautostrada').text();
	idmodify = newStr;
}
//conferma le modificare del casello in insert autostrada
function confermamodifichecaselloinsertautostrada(){
	
	if(document.getElementById('namecasellomodifyinsertautostrada').value === '' || document.getElementById('kmcasellomodifyinsertautostrada').value === '')
	{
		alert("Inserire tutti i campi per modificare il Casello!");
	}
	else
	{
		document.getElementById('modifycaselloinsertautostrada').style.display='none';
		$('#'+idmodify+'idmomentaneonomecaselloinsertautostrada').html(document.getElementById('namecasellomodifyinsertautostrada').value);
		$('#'+idmodify+'idmomentaneokmcaselloinsertautostrada').html(document.getElementById('kmcasellomodifyinsertautostrada').value);
	}
}

//insert autostrada
function insertautostrada(){
	var name = document.getElementById('nameautostradainsertautostrada').value;
	var tip = document.getElementById('typologyautostradainsertautostrada').value;
	var n = [];
	var k = [];
	for(var i = 0; i < r; i++){
		var n2 = $('#'+i+'idmomentaneonomecaselloinsertautostrada').text();
		var k2 = $('#'+i+'idmomentaneokmcaselloinsertautostrada').text();
		n.push(n2);
		k.push(k2);
	}

	if(name === '' || n.length < 2)
	{
		alert("Inserisci tutti i dati per registrare una nuova Autostrada!");
	}
	else
	{
		$.ajax({
			url : 'insertautostrada',
			data : {name, tip, n, k},
			success : function() {location.reload();}
		});	
	}
}
















