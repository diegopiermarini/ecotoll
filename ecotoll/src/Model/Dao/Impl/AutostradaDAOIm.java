package Model.Dao.Impl;


import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

import Framework.DataLayerException;
import Framework.dbmanager;
import Model.Bean.Admin;
import Model.Bean.Autostrada;
import Model.Dao.Interface.AdminDAO;
import Model.Dao.Interface.AutostradaDAO;

public class AutostradaDAOIm implements AutostradaDAO {
	
	private static final String get_autostrade = "SELECT * FROM autostrade";
	private static final String insert_autostrada = "INSERT INTO autostrade (nome_autostrada, tariffa_idtariffa) VALUES (?,?)";
	private static final String get_autostrada = "SELECT * FROM autostrade WHERE nome_autostrada = ? AND tariffa_idtariffa = ?";
	private static final String remove_autostrada = "DELETE FROM  autostrade WHERE id_autostrada = ?";
	private static final String modifyautostrada = "UPDATE autostrade SET nome_autostrada = ?, tariffa_idtariffa = ? WHERE id_autostrada = ?";
	private static final String get_autostradabyid = "SELECT * FROM autostrade WHERE id_autostrada = ?";

	
	/**
	 *  Crea una lista di tulle le autostrade
	 * 
	 * @return lista di oggetti Autostrada <b>(List<Autostrada>)</b> 
	 */
	public List<Autostrada> getAutostrade() throws DataLayerException {
		List<Autostrada> auto = new ArrayList();		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_autostrade)){
				try(ResultSet rs = ps.executeQuery()){
						while(rs.next()) {
							auto.add(
									new Autostrada(rs.getInt("id_autostrada"),
											rs.getString("nome_autostrada"),
											rs.getInt("tariffa_idtariffa"))
							);
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET AUTOSTRADE", ex);
        }		
		return auto;
	}
	
	/**
	 *  Inserisce un autostrada
	 *  
	 * @param a <b>(Autostrada)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int insertAutostrada(Autostrada a) throws DataLayerException {
		int result = -1;
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(insert_autostrada)){
				ps.setString(1, a.getNome());
				ps.setInt(2, a.getTip());
				result = ps.executeUpdate();
				}
		}catch (SQLException ex) {
            throw new DataLayerException("INSERT AUTOSTRADA", ex);
        }			
		return result;
	}

	/**
	 *  Crea un oggetto Autostrada preso dal DataBase  
	 *  
	 * @param n <b>(String)</b>
	 * @param t <b>(Int)</b>
	 * 
	 * @return oggetto Autostrada <b>(Autostrada)</b> 
	 */
	public Autostrada getAutostrada(String n, int t) throws DataLayerException {
		Autostrada auto = null;		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_autostrada)){
				ps.setString(1, n);
				ps.setInt(2, t);
				try(ResultSet rs = ps.executeQuery()){
						if(rs.next()) {
								auto = new Autostrada(rs.getInt("id_autostrada"),
										rs.getString("nome_autostrada"),
										rs.getInt("tariffa_idtariffa"));
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET AUTOSTRADA", ex);
        }		
		return auto;
	}
	
	/**
	 *  Elimina un Autostrada
	 *  
	 * @param id <b>(Int)</b>
	 * 
	 */
	public void removeAutostrada(int id) throws DataLayerException {
		try(Connection conn = dbmanager.getConnection()){
			try(PreparedStatement ps = conn.prepareStatement(remove_autostrada)){
				ps.setInt(1, id);
				ps.executeUpdate();
			}
		}catch(SQLException ex) {
			throw new DataLayerException("RIMOZIONE AUTOSTRADA", ex);
		}
	}
	
	/**
	 *  Modifica un Autostrada   
	 *  
	 * @param name <b>(String)</b>
	 * @param tip <b>(Int)</b>
	 * @param idautostrada <b>(Int)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyAutostrada(String name, int tip, int idautostrada) throws DataLayerException {
		int result = -1;
		
		try (Connection conn = dbmanager.getConnection()){
			
			try(PreparedStatement ps = conn.prepareStatement(modifyautostrada)){
				ps.setString(1, name);
				ps.setInt(2, tip);
				ps.setInt(3, idautostrada);
				
				result=ps.executeUpdate();
			}
		}catch (SQLException e) {
			throw new DataLayerException("MODIFY AUTOSTRADA", e);
		}
		return result;
	}
	
	/**
	 *  Crea un oggetto Autostrada preso dal DataBase  
	 *  
	 * @param id <b>(Int)</b>
	 * 
	 * @return oggetto Autostrada <b>(Autostrada)</b> 
	 */
	public Autostrada getAutostradabyid(int id) throws DataLayerException {
		Autostrada auto = null;		
		try(Connection conn = dbmanager.getConnection()){
			try (PreparedStatement ps = conn.prepareStatement(get_autostradabyid)){
				ps.setInt(1, id);
				try(ResultSet rs = ps.executeQuery()){
						if(rs.next()) {
								auto = new Autostrada(rs.getInt("id_autostrada"),
										rs.getString("nome_autostrada"),
										rs.getInt("tariffa_idtariffa"));
						}
				}
			}
		}catch (SQLException ex) {
            throw new DataLayerException("GET AUTOSTRADA", ex);
        }		
		return auto;
	}

		
	

}
