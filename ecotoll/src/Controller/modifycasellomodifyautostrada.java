package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Bean.Casello;
import Model.Dao.Impl.CaselloDAOIm;

@WebServlet("/modifycasellomodifyautostrada")
public class modifycasellomodifyautostrada extends HttpServlet {
	
	/**
	 * modifica di un casello all'interno della modifica del'autostrada 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String namecasello = request.getParameter("name");
		float kmcasello = Float.valueOf(request.getParameter("km"));
		int idcasello = Integer.parseInt(request.getParameter("idmodify"));
		
		try {
			new CaselloDAOIm().modifyCasello(namecasello, kmcasello, idcasello);
			Casello cas = new CaselloDAOIm().getCaselloidcasello(idcasello);
			
			String z = String.valueOf(cas.getId_autostrada());
			
			response.setContentType("text/plain");
			response.getWriter().write(z.trim());
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
