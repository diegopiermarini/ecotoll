package Model.Dao.Interface;

import java.util.List;

import Framework.DataLayerException;
import Model.Bean.Tariffa;

public interface TariffaDAO {
	
	/**
	 *  Crea una lista delle Tariffe
	 *     
	 * @return lista di oggetti Tariffa <b>(List<Tariffa>)</b>
	 */
	public List<Tariffa> getTariffe() throws DataLayerException;
	
	/**
	 *  Modifica una Tariffa   
	 *  
	 * @param id <b>(Int)</b>
	 * @param tarA <b>(Double)</b>
	 * @param tarB <b>(Double)</b>
	 * @param tar3 <b>(Double)</b>
	 * @param tar4 <b>(Double)</b>
	 * @param tar5 <b>(Double)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyTariffa(int id, double tarA, double tarB, double tar3, double tar4, double tar5) throws DataLayerException;

	/**
	 *  Crea oggetto Tariffa
	 * 
	 * @param id <b>(Int)</b>
	 *     
	 * @return oggetto Tariffa <b>(Tariffa)</b>
	 */
	public Tariffa getTariffabyid(int id) throws DataLayerException;
	
}
