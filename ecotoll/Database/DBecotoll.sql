SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecotoll`
--
 CREATE DATABASE IF NOT EXISTS ecotoll;
 USE ecotoll;
-- --------------------------------------------------------

--
-- Struttura della tabella `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struttura della tabella `autostrade`
--

CREATE TABLE `autostrade` (
  `id_autostrada` int(11) NOT NULL,
  `nome_autostrada` varchar(10) DEFAULT NULL,
  `tariffa_idtariffa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Dump dei dati per la tabella `autostrade`
--

INSERT INTO `autostrade` (`id_autostrada`, `nome_autostrada`, `tariffa_idtariffa`) VALUES
(1, 'A1', 1),(2, 'A2', 1),(3, 'A3', 1),(4, 'A4', 2),(5, 'A5', 2),(6, 'A6', 2),(7, 'A7', 1),(8, 'A8', 2),(9, 'A9', 1),(10, 'A10', 1),
(11, 'A11', 1),(12, 'A12', 1),(13, 'A13', 1),(14, 'A14', 1),(15, 'A15', 1),(16, 'A16', 2),(17, 'A18', 1),(18, 'A19', 2),(19, 'A20', 1),(20, 'A21', 2),
(21, 'A22', 2),(22, 'A23', 2),(23, 'A24', 2),(24, 'A25', 2),(25, 'A26', 2),(26, 'A27', 2),(27, 'A28', 2),(28, 'A29', 1),(29, 'A30', 1),(30, 'A31', 1),
(31, 'A32', 2),(32, 'A33', 2),(33, 'A34', 1),(34, 'A35', 2),(35, 'A36', 2),(36, 'A50', 1),(37, 'A51', 1),(38, 'A52', 1),(39, 'A55', 2),(40, 'A56', 1),
(41, 'A57', 1),(42, 'A58', 1),(43, 'A91', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `caselli`
--

CREATE TABLE `caselli` (
  `id_casello` int(11) NOT NULL,
  `nome_casello` varchar(25) DEFAULT NULL,
  `km_casello` float (5) DEFAULT NULL,
  `autostrada_idautostrada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `caselli`
--

INSERT INTO `caselli` (`id_casello`, `nome_casello`, `km_casello`, `autostrada_idautostrada`) VALUES
-- caselli A1
(1, 'Anagni - Fiuggi Terme', 603, 1),(2, 'Arezzo', 359, 1),(3, 'Asse Mediano - Afragola - Acerra', 749, 1),(4, 'Attigliano', 481, 1),(5, 'Barberino di Mugello', 263, 1),(6, 'Caianello', 700, 1),(7, 'Calenzano - Sesto Fiorentino', 279, 1),(8, 'Capua', 719, 1),(9, 'Casalpusterlengo', 39, 1),(10, 'Caserta Nord', 733, 1),
(11, 'Caserta Sud - Marcianise', 741, 1),(12, 'Cassino', 669, 1),(13, 'Ceprano', 644, 1),(14, 'Chiusi - Chianciano Terme', 411, 1),(15, 'Colleferro', 592, 1),(16, 'Fabro', 429, 1),(17, 'Ferentino', 618, 1),(18, 'Fidenza - Salsomaggiore T.', 91, 1),(19, 'Fiorenzuola', 75, 1),(20, 'Firenze - Impruneta', 295, 1),
(21, 'Firenze - Scandicci', 286, 1),(22, 'Firenze Nord', 281, 1),(23, 'Firenze Sud', 301, 1),(24, 'Frosinone', 624, 1),(25, 'Guidonia Montecelio', 555, 1),(26, 'Incisa - Reggello', 320, 1),(27, 'Lodi', 24, 1),(28, 'Magliano Sabina', 302, 1),(29, 'Melegnano - Binasco', 9, 1),(30, 'Modena Nord', 158, 1),
(31, 'Modena Sud', 171, 1),(32, 'Monte S.Savino', 372, 1),(33, 'Napoli Casoria', 755, 1),(34, 'Napoli Centro Direzionale', 757, 1),(35, 'Orte', 493, 1),(36, 'Orvieto', 452, 1),(37, 'Parma', 111, 1),(38, 'Piacenza Sud', 59, 1),(39, 'Pian del Voglio', 238, 1),(40, 'Pomigliano - Villa Literno', 745, 1),
(41, 'Pontecorvo - Castrocielo', 658, 1),(42, 'Ponzano Romano - Soratte', 516, 1),(43, 'Reggio Emilia', 138, 1),(44, 'Rioveggio', 224, 1),(45, 'Roncobilaccio', 243, 1),(46, 'S.Donato Milanese', 0, 1),(47, 'S.Giuliano Milanese', 3, 1),(48, 'S.Vittore', 678, 1),(49, 'Santa Maria Capua Vetere', 728, 1),(50, 'Sasso Marconi', 208, 1),
(51, 'Sasso Marconi Nord', 201, 1),(52, 'Terre di Canossa - Campegine', 125, 1),(53, 'Val di Chiana - Sinalunga', 387, 1),(54, 'Valdarno', 336, 1),(55, 'Valmontone', 586, 1),(56, 'Valsamoggia', 186, 1),(57, 'Villa Costanza', 288, 1),
-- caselli A2
(58, 'Altilia - Grimaldi', 283, 2),(59, 'Altomonte', 211, 2),(60, 'Arghill�', 433, 2),(61, 'Atena Lucana', 80, 2),(62, 'Bagnara Calabra', 404, 2),(63, 'Battipaglia', 20, 2),(64, 'Buonabitacolo - Padula', 101, 2),(65, 'Campagna', 33, 2),(66, 'Campo Calabro', 432, 2),(67, 'Campotenese', 171, 2),
(68, 'Catanzaro - Lamezia Terme', 317, 2),(69, 'Contursi', 43, 2),(70, 'Cosenza', 256, 2),(71, 'Cosenza Nord', 250, 2),(72, 'Eboli', 29, 2),(73, 'Falerna', 301, 2),(74, 'Frascineto - Castrovillari', 191, 2),(75, 'Gallico', 434, 2),(76, 'Gioia Tauro', 390, 2),(77, 'Lagonegro Nord - Maratea', 121, 2),
(78, 'Lagonegro Sud', 123, 2),(79, 'Laino Borgo', 150, 2),(80, 'Lauria Nord', 135, 2),(81, 'Lauria Sud', 142, 2),(82, 'Mileto',367 , 2),(83, 'Montecorvino - Pontecagnano Sud', 15, 2),(84, 'Morano - Castrovillari', 182, 2),(85, 'Mormanno - Scalea', 160, 2),(86, 'Palmi', 398, 2),(87, 'Petina', 62, 2),
(88, 'Pizzo', 336, 2),(89, 'Polla', 73, 2),(90, 'Pontecagnano', 10, 2),(91, 'Reggio Calabria', 440, 2),(92, 'Reggio Calabria - Porto', 439, 2),(93, 'Rogliano - Grimaldi', 270, 2),(94, 'Rosarno', 380, 2),(95, 'Rose - Montalto Uffugo', 243, 2),(96, 'S.Mango d''Aquino', 291, 2),(97, 'S.Onofrio - Vibo Valentia', 345, 2),
(98, 'Sala Consilina', 85, 2),(99, 'San Mango', 4, 2),(100, 'Santa Trada', 424, 2),(101, 'Scilla', 420, 2),(102, 'Sibari', 205, 2),(103, 'Sicignano', 51, 2),(104, 'Soriano Calabro', 356, 2),(105, 'Tarsia Nord - Spezzano Terme', 217, 2),(106, 'Tarsia Sud', 222, 2),(107, 'Torano', 232, 2),
(108, 'Villa S.Giovanni', 430, 2),
-- caselli A3
(109, 'Angri', 28, 3),(110, 'Angri Sud', 30, 3),(111, 'Boscoreale', 20.5, 3),(112, 'Castellammare di Stabia', 22, 3),(113, 'Cava de Tirreni', 43, 3),(114, 'Ercolano Scavi', 9, 3),(115, 'Napoli', 0, 3),(116, 'Nocera Inferiore', 37, 3),(117, 'Pompei Est - Scafati', 25, 3),(118, 'Pompei Ovest', 21, 3),
(119, 'Portici - Ercolano', 8, 3),(120, 'Portici Sud', 7, 3),(121, 'S.Giovanni a Teduccio', 2, 3),(122, 'Salerno - Fratte', 54, 3),(123, 'Salerno Centro', 52, 3),(124, 'San Giorgio a Cremano Nord', 5, 3),(125, 'San Giorgio a Cremano Sud', 6, 3),(126, 'Torre Annunziata Nord', 17, 3),(127, 'Torre Annunziata Sud', 20, 3),(128, 'Torre del Greco', 12, 3),
(129, 'Vietri sul Mare', 48, 3),
-- caselli A4
(130, 'Agrate', 146, 4),(131, 'Arluno', 111, 4),(132, 'Balocco', 60, 4),(133, 'Bergamo', 173, 4),(134, 'Biandrate - Vicolungo', 74, 4),(135, 'Borgo d''Ale', 37, 4),(136, 'Brandizzo Est', 12, 4),(137, 'Brescia Est', 228, 4),(138, 'Brescia Ovest', 216, 4),(139, 'Capriate', 161, 4),
(140, 'Carisio', 54, 4),(141, 'Cavenago - Cambiago', 151, 4),(142, 'Cessalto', 433, 4),(143, 'Chivasso Centro', 17, 4),(144, 'Chivasso Est', 21, 4),(145, 'Chivasso Ovest', 14, 4),(146, 'Cormano', 130, 4),(147, 'Dalmine', 168, 4),(148, 'Desenzano', 244, 4),(149, 'Duino', 520, 4),
(150, 'Greggio', 67, 4),(151, 'Grisignano', 344, 4),(152, 'Grumello - Telgate', 189, 4),(153, 'Latisana', 466, 4),(154, 'Marcallo - Mesero', 105, 4),(155, 'Martellago - Scorz�', 389, 4),(156, 'Monfalcone', 516, 4),(157, 'Montebello', 312, 4),(158, 'Montecchio', 321, 4),(159, 'Novara Est', 90, 4),
(160, 'Novara Ovest', 83, 4),(161, 'Ospitaletto', 207, 4),(162, 'Padova Est', 364, 4),(163, 'Padova Ovest', 357, 4),(164, 'Palazzolo', 194, 4),(165, 'Palmanova', 492, 4),(166, 'Pero', 123, 4),(167, 'Peschiera del Garda', 260, 4),(168, 'Ponte Oglio', 192, 4),(169, 'Preganziol', 400, 4),
(170, 'Redipuglia', 507, 4),(171, 'Rho', 118, 4),(173, 'Roncade - Meolo', 416, 4),(174, 'Rondissone', 24, 4),(175, 'Rovato', 202, 4),(176, 'S.Giorgio - Porpetto', 483, 4),(177, 'San Don� - Noventa', 425, 4),(178, 'Santhi�', 45, 4),(179, 'Santo Stino di Livenza', 439, 4),
(180, 'Seriate', 180, 4),(181, 'Sesto S.Giovanni', 136, 4),(182, 'Settimo Torinese', 5, 4),(183, 'Sirmione', 251, 4),(184, 'Sistiana', 523, 4),(185, 'Soave - S.Bonifacio', 303, 4),(186, 'Sommacampagna', 271, 4),(187, 'Spinea', 381, 4),(188, 'Torino', 0, 4),(189, 'Trezzo', 159, 4),
(190, 'Verona Est', 290, 4),(191, 'Verona Sud', 281, 4),(192, 'Vicenza Est', 335, 4),(193, 'Vicenza Ovest', 327, 4),(194, 'Volpiano Sud - Brandizzo Ovest', 10, 4),
-- caselli A5
(195, 'Aosta Est', 43, 5),(196, 'Aosta Ovest', 30, 5),(197, 'Courmayeur Nord', 0, 5),(198, 'Courmayeur Sud', 4, 5),(199, 'Ivrea', 106, 5),(200, 'Morgex', 12, 5),(201, 'Nus', 51, 5),(202, 'Pont St. Martin', 86, 5),(203, 'Quincinetto', 91, 5),(204, 'S.Giorgio Canavese', 121, 5),
(205, 'S.Vincent - Chatillon', 62, 5),(206, 'Scarmagno', 112, 5),(207, 'Verres', 74, 5),(208, 'Volpiano', 134, 5),
-- caselli A6
(209, 'Altare - Carcare', 110, 6),(210, 'Carmagnola', 13, 6),(211, 'Carr�', 58, 6),(212, 'Ceva', 81, 6),(213, 'Fossano', 49, 6),(214, 'Marene', 34, 6),(215, 'Millesimo', 97, 6),(216, 'Mondov�', 63, 6),(217, 'Niella Tanaro', 71, 6),
-- caselli A7
(218, 'Assago', 2, 7),(219, 'Binasco', 10, 7),(220, 'Busalla', 111, 7),(221, 'Casei Gerola', 50, 7),(222, 'Castelnuovo Scrivia', 54, 7),(223, 'Genova', 134, 7),(224, 'Genova Bolzaneto', 126, 7),(225, 'Gropello Cairoli', 31, 7),(226, 'Isola del Cantone', 101, 7),(227, 'Milano V.le Famagosta', 0, 7),
(228, 'Ronco Scrivia', 107, 7),(229, 'Serravalle Scrivia', 84, 7),(230, 'Tortona', 66, 7),(231, 'Vignole - Arquata', 89, 7),
-- caselli A8
(232, 'Arese', 36, 8),(233, 'Azzate - Buguggiate', 0, 8),(234, 'Busto Arsizio', 18, 8),(235, 'Castellanza', 24, 8),(236, 'Castronno', 3, 8),(237, 'Cavaria', 9, 8),(238, 'Gallarate', 12, 8),(239, 'Gazzada', 1, 8),(240, 'Lainate', 34, 8),(241, 'Legnano', 26, 8),
(242, 'Milano Fiera', 40, 8),(243, 'Milano Viale Certosa', 43, 8),(244, 'Solbiate Arno', 7, 8),
-- caselli A9
(245, 'Como M. Olimpino', 5, 9),(246, 'Como Nord', 2, 9),(247, 'Como Sud', 9, 9),(248, 'Fino Mornasco', 13, 9),(249, 'Lomazzo', 18, 9),(250, 'Origgio - Uboldo', 31, 9),(251, 'Saronno', 27, 9),(252, 'Turate', 24, 9),
-- caselli A10
(253, 'Albisola', 36, 10),(254, 'Andora', 93, 10),(255, 'Arenzano', 20, 10),(256, 'Arma di Taggia', 128, 10),(257, 'Bordighera', 146, 10),(258, 'Borghetto S.Spirito', 72, 10),(259, 'Celle Ligure', 32, 10),(260, 'Finale Ligure', 63, 10),(261, 'Genova Aereoporto', 2, 10),(262, 'Genova Pegli',6 , 10),
(263, 'Genova Pr�', 11, 10),(264, 'Imperia Est', 106, 10),(265, 'Imperia Ovest', 111, 10),(266, 'Orco Feglino', 59, 10),(267, 'Pietra Ligure', 68, 10),(268, 'S.Bartolomeo al Mare', 100, 10),(269, 'Sanremo Ovest', 139, 10),(270, 'Savona - Vado', 44, 10),(271, 'Spotorno', 53, 10),(272, 'Varazze', 27, 10),
(273, 'Ventimiglia', 151, 10),(274, 'Albenga', 81, 10),
-- caselli A24
(275, 'Assergi', 32, 23),(276, 'Basciano - Villa Vomano', 0, 23),(277, 'Carsoli - Oricola', 98, 23),(278, 'Castel Madama', 125, 23),(279, 'Colledara - S.Gabriele', 12, 23),(280, 'L''Aquila Est', 42, 23),(281, 'L''Aquila Ovest', 47, 23),(282, 'Roma', 150, 23),(283, 'Tagliacozzo', 86, 23),(284, 'Tivoli', 136, 23),
(285, 'Tornimparte', 64, 23),(286, 'Valle del Salto', 72, 23),(287, 'Vicovaro - Mandela', 116, 23);

-- --------------------------------------------------------

--
-- Struttura della tabella `veicolo`
--

CREATE TABLE `veicolo` (
  `id_veicolo` int(5) NOT NULL,
  `modello` varchar(25) DEFAULT NULL,
  `marca` varchar(25) DEFAULT NULL,
  `anno` int(4) DEFAULT NULL,
  `targa` varchar(7) DEFAULT NULL,
  `assi` int(1) DEFAULT NULL,
  `peso` float(7) DEFAULT NULL,
  `altezza` float(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `veicolo`
--
INSERT INTO `veicolo` (`id_veicolo`, `modello`, `marca`, `anno`, `targa`, `assi`, `peso`, `altezza`) VALUES
(1, 'Giulietta', 'Alfa Romeo', 2000, 'YU854ND', 2, 1280, 1.15),
(2, 'A4', 'Audi', 2004, 'FR823TG', 2, 1485, 2.55),
(3, 'Astra', 'Opel', 2010, 'LK218JH', 3, 1273, 2.10),
(4, 'Clio', 'Renault', 2015, 'CV284AO', 4, 1090, 2.79),
(5, 'Punto', 'Fiat', 2018, 'SD202FR', 5, 1025, 3.50),
(6, '999', 'Ducati', 2013, 'CV397EF', 0, 186, 0.78);

-- --------------------------------------------------------

--
-- Struttura della tabella `tariffe`
--

CREATE TABLE `tariffe` (
  `id_tariffa` int(11) NOT NULL,
  `tipologia` varchar(10) DEFAULT NULL,
  `A` double DEFAULT NULL,
  `B` double DEFAULT NULL,
  `tre` double DEFAULT NULL,
  `quattro` double DEFAULT NULL,
  `cinque` double DEFAULT NULL  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `tariffe`
--
INSERT INTO `tariffe` (`id_tariffa`, `tipologia`, `A`, `B`, `tre`, `quattro`, `cinque`) VALUES
(1, 'pianura', 0.0564018, 0.0577278, 0.0769236, 0.1159392, 0.136734),
(2, 'montagna', 0.0666666, 0.0682422, 0.0890292, 0.1359228, 0.1609062);
-- --------------------------------------------------------

--
-- Struttura della tabella `iva`
--

CREATE TABLE `iva` (
  `id_iva` int(11) NOT NULL,
  `valore` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `tariffe`
--
INSERT INTO `iva` (`id_iva`, `valore`) VALUES
(1, 22.00);
-- --------------------------------------------------------

--
-- Indici PRIMARY KEY
--

--
-- Indici per le tabelle `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);
  
--
-- Indici per le tabelle `autostrade`
--
ALTER TABLE `autostrade`
  ADD PRIMARY KEY (`id_autostrada`),
  ADD KEY `tariffa_idtariffa` (`tariffa_idtariffa`);

--
-- Indici per le tabelle `caselli`
--
ALTER TABLE `caselli`
  ADD PRIMARY KEY (id_casello),
  ADD KEY `autostrada_idautostrada` (`autostrada_idautostrada`);
  
--
-- Indici per le tabelle `veicolo`
--
ALTER TABLE `veicolo`
  ADD PRIMARY KEY (id_veicolo);
  
--
-- Indici per le tabelle `tariffe`
--
ALTER TABLE `tariffe`
  ADD PRIMARY KEY (id_tariffa);



--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
  
--
-- AUTO_INCREMENT per la tabella `autostrade`
--
ALTER TABLE `autostrade`
  MODIFY `id_autostrada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `caselli`
--
ALTER TABLE `caselli`
  MODIFY `id_casello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
  
--
-- AUTO_INCREMENT per la tabella `veicolo`
--
ALTER TABLE `veicolo`
  MODIFY `id_veicolo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `tariffe`
--
ALTER TABLE `tariffe`
  MODIFY `id_tariffa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `autostrade`
--
ALTER TABLE `autostrade`
  ADD CONSTRAINT `autostrade_ibfk_1` FOREIGN KEY (`tariffa_idtariffa`) REFERENCES `tariffe` (`id_tariffa`);

--
-- Limiti per la tabella `caselli`
--
ALTER TABLE `caselli`
  ADD CONSTRAINT `caselli_ibfk_1` FOREIGN KEY (`autostrada_idautostrada`) REFERENCES `autostrade` (`id_autostrada`);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;