//var di appoggio
var iddelete = "";
//visualizzare il modal per eliminare il casello 
function viewmodaldeletecasello(x){
	
	var name = $('#'+x).text();
	document.getElementById('deletecasello').style.display='block';
	document.getElementById("namecasellodelete").innerHTML = "Do you really want to delete these Casello <u style='color:red;'>"+name+"</u> ?";
	iddelete = x;
}
//elimina casello 
function deletecaselo(){
	
	var idcasello = iddelete.replace(/casellonome/,"");
	$.ajax({
		url : 'deletecasello',
		data : {idcasello},
		success : function() {location.reload();}
	});		
}
//var di appoggio
var idmodify = "";
//visualizzare il modal per modificare il casello in modify autostrada
function viewmodalmodifycasello(y){
	
	document.getElementById('modifycasello').style.display='block';
	document.getElementById('namecasellomodify').value = $('#'+y+'casellonome').text();
	document.getElementById('kmcasellomodify').value = $('#'+y+'casellokm').text();
	idmodify = y;
}
//conferma le modificare del casello 
function confermamodifichecasello(){
	
	if(document.getElementById('namecasellomodify').value === '' || document.getElementById('kmcasellomodify').value === '')
	{
		alert("Inserire tutti i campi per modificare il Casello!");
	}
	else
	{
		var name = document.getElementById('namecasellomodify').value;
		var km = document.getElementById('kmcasellomodify').value
		$.ajax({
			url : 'modifycasello',
			data : {name, km, idmodify},
			success : function(responseText) {location.reload();}
		
		});
	}
}