package Controller;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Bean.Autostrada;
import Model.Bean.Casello;
import Model.Dao.Impl.AutostradaDAOIm;
import Model.Dao.Impl.CaselloDAOIm;

@WebServlet("/insertautostrada")
public class insertautostrada extends HttpServlet {


	/**
	 * Inserisce una nuova autostrada con i relativi caselli
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Autostrada auto = new Autostrada(request.getParameter("name"), Integer.parseInt(request.getParameter("tip")));
		String[] nc = request.getParameterValues("n[]");
		String[] kc = request.getParameterValues("k[]");
		
		try {
			new AutostradaDAOIm().insertAutostrada(auto);
			Autostrada auto2 = new AutostradaDAOIm().getAutostrada(auto.getNome(), auto.getTip());
			int id_auto = auto2.getId();
			for(int i = 0; i < nc.length; i++) {
				Casello cas = new Casello(nc[i], Float.valueOf(kc[i]), id_auto);
				new CaselloDAOIm().insertCasello(cas);
			}
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
