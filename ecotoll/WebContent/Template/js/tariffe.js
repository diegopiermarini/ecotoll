//var di appoggio
var idmodify = "";
//visualizzare il modal per modificare le tariffe
 function viewmodalmodifytariffe(x){
	
	document.getElementById('modifytariffe').style.display='block';
	document.getElementById("modifytariffaTipologia").innerHTML = $('#'+x+'tariffaTipologia').text();
	document.getElementById("modifytariffaA").value = $('#'+x+'tariffaA').text(); 
	document.getElementById("modifytariffaB").value = $('#'+x+'tariffaB').text();
	document.getElementById("modifytariffa3").value = $('#'+x+'tariffa3').text();
	document.getElementById("modifytariffa4").value = $('#'+x+'tariffa4').text();
	document.getElementById("modifytariffa5").value	= $('#'+x+'tariffa5').text();	
	idmodify = x;
}
//conferma le modificare delle tariffe
 function confermamodifichetariffe(){
 	
 	if(document.getElementById("modifytariffaA").value === '' || document.getElementById("modifytariffaB").value === '' || document.getElementById("modifytariffa3").value === '' || document.getElementById("modifytariffa4").value === '' || document.getElementById("modifytariffa5").value === '')
 	{
 		alert("Inserire tutti i campi per modificare la Tariffa!");
 	}
 	else
 	{
 		var id = idmodify;
 		var tarA = document.getElementById("modifytariffaA").value.replace(/,/,".");
 		var tarB = document.getElementById("modifytariffaB").value.replace(/,/,".");
 		var tar3 = document.getElementById("modifytariffa3").value.replace(/,/,".");
 		var tar4 = document.getElementById("modifytariffa4").value.replace(/,/,".");
 		var tar5 = document.getElementById("modifytariffa5").value.replace(/,/,".");
 		$.ajax({
			url : 'modifytariffe',
			data : {id, tarA, tarB, tar3, tar4, tar5},
			success : function() {
				
				document.getElementById('modifytariffe').style.display='none';
				
				$('#'+idmodify+'tariffaA').html(document.getElementById("modifytariffaA").value); 
				$('#'+idmodify+'tariffaB').html(document.getElementById("modifytariffaB").value);
				$('#'+idmodify+'tariffa3').html(document.getElementById("modifytariffa3").value);
				$('#'+idmodify+'tariffa4').html(document.getElementById("modifytariffa4").value);
				$('#'+idmodify+'tariffa5').html(document.getElementById("modifytariffa5").value);		
			}
		});	
 	}
 }