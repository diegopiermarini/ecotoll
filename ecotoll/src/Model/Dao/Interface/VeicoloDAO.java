package Model.Dao.Interface;

import java.util.List;

import Framework.DataLayerException;
import Model.Bean.Veicolo;

public interface VeicoloDAO {

	/**
	 *  Crea una lista di tutti i Veicolo
	 * 
	 * @return lista di oggetti Veicolo <b>(List<Veicolo>)</b> 
	 */
	public List<Veicolo> getVeicoli() throws DataLayerException;
	
	/**
	 *  Crea una oggetto Veicolo
	 *  
	 *  @param id <b>(Int)</b>
	 * 
	 * @return oggetto Veicolo <b>(Veicolo)</b> 
	 */
	public Veicolo getVeicolobyid(int id) throws DataLayerException;
}
