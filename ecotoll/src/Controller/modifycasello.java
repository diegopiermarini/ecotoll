package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Dao.Impl.CaselloDAOIm;

@WebServlet("/modifycasello")
public class modifycasello extends HttpServlet {
	
	/**
	 * Modifica un casello
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String namecasello = request.getParameter("name");
		float kmcasello = Float.valueOf(request.getParameter("km"));
		int idcasello = Integer.parseInt(request.getParameter("idmodify"));
		
		try {
			new CaselloDAOIm().modifyCasello(namecasello, kmcasello, idcasello);
		} catch (DataLayerException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
