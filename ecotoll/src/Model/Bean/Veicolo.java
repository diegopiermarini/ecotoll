package Model.Bean;

public class Veicolo {
	String modello;
	String marca;
	int anno; 
	String targa;
	int assi;
	double peso;
	double altezza;
	int id_veicolo;
	
	/**
	 * Costruttore classe Veicolo
	 * Inizializzazione dei dati Veicolo presi dal DataBase per il calcolo del pedaggio
	 * 
	 * @param id_veicolo <b>(Int)</b>
	 * @param modello <b>(String)</b>
	 * @param marca <b>(String)</b>
	 * @param anno <b>(Int)</b>
	 * @param targa <b>(String)</b>
	 * @param assi <b>(Int)</b>
	 * @param peso <b>(Double)</b>
	 * @param altezza <b>(Double)</b>
	 */
	public Veicolo(int id_veicolo, String modello, String marca, int anno, String targa, int assi, double peso, double altezza){
		this.id_veicolo=id_veicolo;
		this.modello=modello;
		this.marca=marca;
		this.anno=anno;
		this.targa=targa;
		this.assi=assi;
		this.peso=peso;
		this.altezza=altezza;	
	}

	public String getModello() {
		return modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public String getTarga() {
		return targa;
	}

	public void setTarga(String targa) {
		this.targa = targa;
	}

	public int getAssi() {
		return assi;
	}

	public void setAssi(int assi) {
		this.assi = assi;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltezza() {
		return altezza;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	public int getId_veicolo() {
		return id_veicolo;
	}

	public void setId_veicolo(int id_veicolo) {
		this.id_veicolo = id_veicolo;
	}

	

}
