package Model.Dao.Interface;

import java.util.List;

import Framework.DataLayerException;
import Model.Bean.Autostrada;

public interface AutostradaDAO {
	
	/**
	 *  Crea una lista di tulle le autostrade
	 * 
	 * @return lista di oggetti Autostrada <b>(List<Autostrada>)</b> 
	 */
	public List<Autostrada> getAutostrade() throws DataLayerException;
	
	/**
	 *  Inserisce un autostrada
	 *  
	 * @param a <b>(Autostrada)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int insertAutostrada(Autostrada a) throws DataLayerException;
	
	/**
	 *  Crea un oggetto Autostrada preso dal DataBase  
	 *  
	 * @param n <b>(String)</b>
	 * @param t <b>(Int)</b>
	 * 
	 * @return oggetto Autostrada <b>(Autostrada)</b> 
	 */
	public Autostrada getAutostrada(String n, int t) throws DataLayerException;
	
	/**
	 *  Elimina un Autostrada
	 *  
	 * @param id <b>(Int)</b>
	 * 
	 */
	public void removeAutostrada(int id) throws DataLayerException;
	
	/**
	 *  Modifica un Autostrada   
	 *  
	 * @param name <b>(String)</b>
	 * @param tip <b>(Int)</b>
	 * @param idautostrada <b>(Int)</b>
	 * 
	 * @return intero <b>(Int)</b> per check su query
	 */
	public int modifyAutostrada(String name, int tip, int idautostrada) throws DataLayerException;

	/**
	 *  Crea un oggetto Autostrada preso dal DataBase  
	 *  
	 * @param id <b>(Int)</b>
	 * 
	 * @return oggetto Autostrada <b>(Autostrada)</b> 
	 */
	public Autostrada getAutostradabyid(int id) throws DataLayerException;
	
}
