package Controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Framework.DataLayerException;
import Model.Bean.Casello;
import Model.Bean.Veicolo;
import Model.Dao.Impl.CaselloDAOIm;
import Model.Dao.Impl.VeicoloDAOIm;

@WebServlet("/checkdaticalcolopedaggio")
public class checkdaticalcolopedaggio extends HttpServlet {
   
	/**
	 * controlla se i dati inseriti nella index sono corretti
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name_targ = request.getParameter("val3");
		boolean check = false;
		String z = "null";
		
		try {
			List<Veicolo> listaveicoli = new VeicoloDAOIm().getVeicoli();
            Iterator<Veicolo> itr = listaveicoli.iterator();
            while(itr.hasNext()) {
            	if(itr.next().getTarga().equals(name_targ))
            	{
            		check = true;
            		break;
            	}          	
            }
            
            
            if(check) 
            // caso in cui la targa inserita esiste
            {
            	String name_ing = request.getParameter("val1");
            	boolean check2 = false;
        		String name_usc = request.getParameter("val2");
        		boolean check3 = false;
        		
            	List<Casello> listaceselli = new CaselloDAOIm().getCaselli();
            	Iterator<Casello> itrcaselli = listaceselli.iterator();
            	Iterator<Casello> itrcaselli2 = listaceselli.iterator();
            	
                while(itrcaselli.hasNext()) {
                	if(itrcaselli.next().getNome().equals(name_ing))
                	{
                		check2 = true;
                	}
                }
                while(itrcaselli2.hasNext()) {
                	if(itrcaselli2.next().getNome().equals(name_usc))
                	{
                		check3 = true;
                	}
                }
                
                if(check2 && check3)
                // caso in cui i caselli inseriti esistono
                {}
                else
                // caso in cui uno\entrambi i caselli inseriti non esiste
                {
                	z = "Uno, o entrambi, dei due caselli inseriti non esiste o non � stato inserito correttamente!";
                }
            }
            else
            // caso in cui la targa inserita non esiste
            {
            	z = "La targa insierita non esiste o non � stata inserita correttamente!";
            }
            	
            response.setContentType("text/plain");
			response.getWriter().write(z.trim());
   
		} catch (DataLayerException e) {
			
			e.printStackTrace();
		}		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
