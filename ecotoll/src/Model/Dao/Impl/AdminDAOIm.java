package Model.Dao.Impl;


import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;

import Framework.DataLayerException;
import Framework.dbmanager;
import Model.Bean.Admin;
import Model.Dao.Interface.AdminDAO;

public class AdminDAOIm implements AdminDAO {
	
	private static final String credenziali = "SELECT * FROM admin WHERE username=? AND password=?";
		
	/**
	 *  Controlla se le credenziali se sono giuste per la login al backend
	 * 
	 * @param username <b>(String)</b>
	 * @param password <b>(String)</b>
	 * 
	 * @return oggetto Admin <b>(Admin)</b> se le credenziali sono giuste
	 */
	public Admin Credenziali(String username, String password) throws DataLayerException {
		Admin admin = null;
        try (Connection connection = dbmanager.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(credenziali)) {
                //Prepare statement
                ps.setString(1, username);
                ps.setString(2, password);
                try (ResultSet rset = ps.executeQuery()) {

                    if (rset.next()) {
                        admin = new Admin(
                                rset.getString("username"),
                                rset.getString("password")
                        );
                    }
                }
            }
        } catch (SQLException ex) {
        	throw new DataLayerException("Connessione al DB non riuscita!", ex);
        }
		return admin;
	}

}
